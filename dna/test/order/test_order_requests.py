import os
from urllib.parse import urlencode
from test_tools import *

from django.test import TestCase

fx_man = FixtureManager()
fx_man.load(fixture_file='data_request')


class TestOrderSequence(TestCase):

    fixtures = [os.path.dirname(__file__) + '/fixtures/db_data.json', ]

    url = '/order'
    url_look = '/order/1'

    headers = {
        'HTTP_CONTENT_TYPE': 'application/x-www-form-urlencoded'
    }

    def setUp(self):

        self.client.post('/auth/login', {
            'username': 'tester',
            'password': 'test'
        })

    def test_list_orders_status_code(self):

        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 200)

    def test_retrieve_order_status_code(self):

        r = self.client.get(self.url_look)
        self.assertEqual(r.status_code, 200)

    @data_provider(fx_man['create_request'])
    def test_create_order_status_code(self, data):

        r = self.client.post(self.url, data)
        self.assertEqual(r.status_code, 201)

    @data_provider(fx_man['update_request'])
    def test_update_order_status_code(self, data):

        r = self.client.put(self.url_look, urlencode(data), **self.headers)
        self.assertEqual(r.status_code, 200)

    @data_provider(fx_man['update_request'])
    def test_destroy_order_status_code(self, data):

        r = self.client.delete(self.url_look)
        self.assertEqual(r.status_code, 204)
