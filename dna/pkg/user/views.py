from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from dna.pkg.user.serializers import UserSerializer, LoginSerializer
from dna.pkg.user.permissions import GuestPermission


class LoginView(GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):

        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        account = authenticate(
            username=serializer.validated_data.get('username'),
            password=serializer.validated_data.get('password')
        )

        if account is not None and account.is_active:
            login(request, account)
            serializer = self.get_serializer(account)

            return Response(serializer.data)

        return Response({
            'message': 'Invalid username or password'
        }, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(GenericAPIView):
    @staticmethod
    def get(request):
        logout(request=request)
        return Response(status=status.HTTP_200_OK)


class RegisterView(GenericAPIView):
    serializer_class = UserSerializer
    permission_classes = (GuestPermission, )

    def post(self, request):

        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data

        username = data.get('username')
        password = data.get('password')
        email = data.get('email')

        user = User.objects.create_user(username,
                                        password=password,
                                        email=email)

        serialized_user = UserSerializer(user)
        return Response(serialized_user.data,
                        status=status.HTTP_201_CREATED)
