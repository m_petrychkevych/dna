from rest_framework import permissions


__author__ = 'm.petrychkevych'
__all__ = ('GuestPermission', )


class GuestPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return not request.user.is_authenticated()
