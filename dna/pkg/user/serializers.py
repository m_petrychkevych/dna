from rest_framework import serializers
from django.contrib.auth.models import User

__author__ = 'm.petrychkevych'
__all__ = ('UserSerializer', 'LoginSerializer', )


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password', )
        write_only_fields = ('password', )


class LoginSerializer(UserSerializer):

    username = serializers.CharField(required=True)
