from django.db import models
from django.contrib.auth.models import User

__author__ = 'm.petrychkevych'
__all__ = ('Order', )


class Order(models.Model):

    STATUS_RECEIVED = 'received'
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_COMPLETED = 'completed'
    STATUS_SHIPPED = 'shipped'

    STATUS_CHOICES = (
        (STATUS_RECEIVED, 'Received'),
        (STATUS_IN_PROGRESS, 'In progress'),
        (STATUS_COMPLETED, 'Completed'),
        (STATUS_SHIPPED, 'Shipped'),
    )

    name = models.CharField(max_length=512)
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=128,
                              default=STATUS_RECEIVED)

    class Meta:
        db_table = 'order'
        verbose_name = 'Order'


class OrderSequence(models.Model):

    name = models.CharField(max_length=50)
    library = models.TextField()
    vector = models.TextField()
    upstream = models.TextField()
    downstream = models.TextField()
    order = models.ForeignKey(Order)

    class Meta:
        db_table = 'order_sequence'
        verbose_name = 'Order Sequence'
