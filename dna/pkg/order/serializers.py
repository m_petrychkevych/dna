from rest_framework.serializers import ModelSerializer, CharField

from dna.pkg.order.models import Order, OrderSequence


__author__ = 'm.petrychkevych'
__all__ = ('OrderSerializer', 'OrderSequenceSerializer', )


class OrderSerializer(ModelSerializer):

    status = CharField(required=False)

    class Meta:
        model = Order


class OrderSequenceSerializer(ModelSerializer):

    class Meta:
        model = OrderSequence
