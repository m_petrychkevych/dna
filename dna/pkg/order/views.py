from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from dna.pkg.order.serializers import OrderSerializer, OrderSequenceSerializer
from dna.pkg.order.models import Order, OrderSequence


__author__ = 'm.petrychkevych'
__all__ = ('OrderViewSet', 'OrderSequenceViewSet', )


class OrderViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def _update_request(self):
        # In tests environment the request.data is mutable. But if we use
        # manage.py runserver - it's become to immutable
        request = self.request
        old_mutable = request.data._mutable
        request.data._mutable = True
        request.data.update({'user': request.user.pk})
        request.data._mutable = old_mutable

    def create(self, request, *args, **kwargs):
        self._update_request()
        return super(OrderViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        self._update_request()
        return super(OrderViewSet, self).update(request, *args, **kwargs)


class OrderSequenceViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = OrderSequenceSerializer

    queryset = OrderSequence.objects.all()

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super(OrderSequenceViewSet, self).update(request,
                                                        *args, **kwargs)
