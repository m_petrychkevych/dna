# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=512)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(max_length=128, default='received', choices=[('received', 'Received'), ('in_progress', 'In progress'), ('completed', 'Completed'), ('shipped', 'Shipped')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'order',
                'verbose_name': 'Order',
            },
        ),
        migrations.CreateModel(
            name='OrderSequence',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('library', models.TextField()),
                ('vector', models.TextField()),
                ('upstream', models.TextField()),
                ('downstream', models.TextField()),
                ('order', models.ForeignKey(to='order.Order')),
            ],
            options={
                'db_table': 'order_sequence',
                'verbose_name': 'Order Sequence',
            },
        ),
    ]
