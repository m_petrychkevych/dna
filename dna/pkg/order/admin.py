from django.contrib import admin

from dna.pkg.order.models import Order, OrderSequence


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'date', 'status', )
    search_fields = ('user__username', )


@admin.register(OrderSequence)
class OrderSequenceAdmin(admin.ModelAdmin):
    list_display = ('name', 'library', 'vector', 'upstream', 'downstream',
                    'order', )
