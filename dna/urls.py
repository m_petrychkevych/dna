from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers

from dna.pkg.order.views import OrderViewSet, OrderSequenceViewSet
from dna.pkg.user.views import LoginView, LogoutView, RegisterView

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'order', OrderViewSet, base_name='order')
router.register(r'sequence', OrderSequenceViewSet, base_name='sequence')

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),

    url(r'^auth/login', LoginView.as_view(), name='auth_login'),
    url(r'^auth/logout', LogoutView.as_view(), name='auth_logout'),
    url(r'^auth/register', RegisterView.as_view(), name='auth_register'),

] + router.urls

