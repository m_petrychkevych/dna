CREATE DATABASE dna_db ENCODING 'utf-8';
CREATE USER dna_user WITH password 'NvS!40f|BxbMQIFp8=K@vI0+4gDa56dB';
GRANT ALL privileges ON DATABASE dna_db TO dna_user;
ALTER DATABASE dna_db OWNER TO dna_user;

CREATE DATABASE test_dna_db ENCODING 'utf-8';
CREATE USER dna_test_user WITH password 'QtGWG2eC92QwxrSU^)o7rded';
GRANT ALL privileges ON DATABASE test_dna_db TO dna_test_user;
ALTER DATABASE test_dna_db OWNER TO dna_test_user;
ALTER USER dna_test_user CREATEDB;