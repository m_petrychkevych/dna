# API

## Registration user

Method: POST

Url: /uth/register

Params: **username, password, email**

Required params: **username, password**

### Example:

    requests.post('/auth/register, data={
        'username': 'new_user',
        'password': '12345',
        'email': 'user@domain.com',
    })
    
### Response:

Status: 201

    {"username":"test_user","email":"test@test.ua","id":2}

## Login user

Method: POST

Url: /auth/login

Params: **username, password**

Required params: **username, password**

### Example:

    requests.post('/auth/login', data={
        'username': 'new_user',
        'password': '12345'
    })

### Response: 

Status: 200

    {"username":"test_user","email":"test@test.ua","id":2}

## Logout user

Method: GET

Url: /auth/logout

### Example:

    requests.post('/auth/logout')

### Response: 

Status: 200

## Create order

Method: POST

Url: /order

Params: **name**

Required params: **name**

### Example:

    requests.post('/order', data={
        'name': 'Name order'
    })

### Response: 

Status: 201

    {id":1,"name":"Test","date":"2015-07-22T18:30:24.981847Z","status":"received","user":1}
    
## Update order

Method: PUT

Url: /order/{id}

Params: **name, status, date**

### Example:

    requests.put('/order/1', urlencode({
        'name': 'Name order'
    }))

### Response: 

Status: 200

    {id":1,"name":"Test","date":"2015-07-22T18:30:24.981847Z","status":"received","user":1}
    
## List orders

Method: GET

Url: /order

### Example:

    requests.get('/order')

### Response: 

Status: 200

    {"results":[{id":1,"name":"Order Name","user":1,"status":"received","date":"2015-07-13T10:06:10.589000Z"}],"next":null,"count":1,"previous":null}
            
## Detail order

Method: GET

Url: /order/{id}

### Example:

    requests.get('/order/1')

### Response: 

Status: 200

    {"id":1,"name":"Test","date":"2015-07-22T18:30:24.981847Z","status":"received","user":1}
      
## Create order sequence

Method: POST

Url: /sequence

Params: **name, order, library, vector, upstream, downstream**

Required params: **name, order, library, vector, upstream, downstream**

### Example:

    requests.post('/sequence', data={
        "order": 1,
        "name": "Test",
        "library": "Test",
        "vector": "Test",
        "upstream": "Test",
        "downstream": "Test"
    })

### Response: 

Status: 201

    {"upstream":"Test","id":2,"name":"Test","library":"Test","order":1,"downstream":"Test","vector":"Test"}
    
## Update order sequence

Method: PUT

Url: /sequence/{id}

Params: **name, vector, library, upstream, downstream**

### Example:

    requests.put('/sequence/1', urlencode({
        'name': 'Test',
        'downstream': 'Test'
    }))

### Response: 

Status: 200

    {"upstream":"Test","id":2,"name":"Test","library":"Test","order":1,"downstream":"Test","vector":"Test"}
    
## List order sequences

Method: GET

Url: /sequence

### Example:

    requests.get('/sequence')

### Response: 

Status: 200

    {"results":[{"upstream":"Test","id":2,"name":"Test","library":"Test","order":1,"downstream":"Test","vector":"Test"}],"next":null,"count":1,"previous":null}
            
## Detail order sequence

Method: GET

Url: /sequence/{id}

### Example:

    requests.get('/sequence/1')

### Response: 

Status: 200

    {"upstream":"Test","id":2,"name":"Test","library":"Test","order":1,"downstream":"Test","vector":"Test"}
